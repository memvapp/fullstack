# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.conf.urls import url, include

from apps.student.views import StudentList, StudentCreate, StudentUpdate, StudentDelete, index, StudentRedAverage, StudentAverage

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^nuevo$', StudentCreate.as_view(), name='student_create'),
    url(r'^listar', StudentList.as_view(), name='student_list'),
    url(r'^editar/(?P<pk>\d+)/$', StudentUpdate.as_view(), name='student_edit'),
    url(r'^eliminar/(?P<pk>\d+)/$', StudentDelete.as_view(), name='student_delete'),
    url(r'^promedioRojoList', StudentRedAverage, name='student_red_average'),
    url(r'^promedios', StudentAverage, name='student_average'),
]