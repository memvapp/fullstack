from __future__ import unicode_literals, absolute_import
from django import forms
from apps.student.models import Student


class StudentForm(forms.ModelForm):

	class Meta:
		model = Student

		fields = [
			'name',
			'Course',
		]
		labels = {
			'nombre': 'Nombre',
			'cursos': 'Cursos'
		}
		widgets = {
			'name': forms.TextInput(attrs={'class':'form-control'}),
			'Course': forms.CheckboxSelectMultiple(),
		}