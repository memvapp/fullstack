# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib import admin
from apps.exam.models import Exam, Score

# Register your models here.

admin.site.register(Exam)
admin.site.register(Score)