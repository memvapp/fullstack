from __future__ import unicode_literals, absolute_import
from django.conf.urls import url, include

from apps.exam.views import index, ExamList, ExamCreate, ExamUpdate, ExamDelete \
,ScoreCreate, ScoreUpdate, ScoreList, ScoreDelete 

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^nuevo$', ExamCreate.as_view(), name='exam_create'),
    url(r'^listar', ExamList.as_view(), name='exam_list'),
    url(r'^editar/(?P<pk>\d+)/$', ExamUpdate.as_view(), name='exam_edit'),
    url(r'^eliminar/(?P<pk>\d+)/$', ExamDelete.as_view(), name='exam_delete'),
    url(r'^nota/nuevo$', ScoreCreate.as_view(), name='score_create'),
    url(r'^nota/listar', ScoreList.as_view(), name='score_list'),
    url(r'^nota/editar/(?P<pk>\d+)/$', ScoreUpdate.as_view(), name='score_edit'),
    url(r'^nota/eliminar/(?P<pk>\d+)/$', ScoreDelete.as_view(), name='score_delete'),
]