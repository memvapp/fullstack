# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django import forms

from apps.course.models import Course

class CourseForm(forms.ModelForm):

	class Meta:
		model = Course
		fields = [
			'name',
		]
		labels = {
			'nombre': 'Nombre',
		}
		widgets = {
			'name':forms.TextInput(attrs={'class':'form-control'}),
		}