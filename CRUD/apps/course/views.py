# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from apps.course.forms import CourseForm
from apps.course.models import Course

def index(request):
	return render(request, 'course/index.html')

class CourseList(ListView):
	model = Course
	template_name = 'course/course_list.html'
	paginate_by = 10

class CourseCreate(CreateView):
	model = Course
	form_class = CourseForm
	template_name = 'course/course_form.html'
	success_url = reverse_lazy('course:course_list')


class CourseUpdate(UpdateView):
	model = Course
	form_class = CourseForm
	template_name = 'course/course_form.html'
	success_url = reverse_lazy('course:course_list')


class CourseDelete(DeleteView):
	model = Course
	template_name = 'Course/course_delete.html'
	success_url = reverse_lazy('course:course_list')