# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from apps.course.models import Course
from apps.student.models import Student

# Create your models here.

class Exam(models.Model):
	title = models.CharField(max_length=255)
	Course = models.ForeignKey(Course, null=True, blank=True, on_delete=models.CASCADE)

	def __str__(self):
		return '{}'.format(self.title)
	
class Score(models.Model):
	Student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
	Exam = models.ForeignKey(Exam, null=True, blank=True, on_delete=models.CASCADE)
	score = models.IntegerField()