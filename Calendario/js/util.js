
/**
 * [getJsonData Obtiene un json]
 * @return {json} [Retorna un parse Json]
 */
function getJsonData(){
	try{
		return JSON.parse('{"monday":[{"name":"Jorge","start_time":"08:00","end_time":"09:00"},{"name":"Jorge","start_time":"09:30","end_time":"11:00"},{"name":"Jorge","start_time":"15:00","end_time":"16:00"},{"name":"Jorge","start_time":"17:00","end_time":"19:30"}],"tuesday":[{"name":"Jorge","start_time":"08:00","end_time":"09:00"},{"name":"Jorge","start_time":"11:30","end_time":"12:00"},{"name":"Jorge","start_time":"15:00","end_time":"16:00"},{"name":"Jorge","start_time":"17:00","end_time":"19:30"}],"wednesday":[{"name":"Jorge","start_time":"08:00","end_time":"09:00"},{"name":"Jorge","start_time":"10:30","end_time":"12:00"},{"name":"Jorge","start_time":"15:00","end_time":"16:00"},{"name":"Jorge","start_time":"17:00","end_time":"19:30"}],"thursday":[{"name":"Jorge","start_time":"08:00","end_time":"09:00"},{"name":"Jorge","start_time":"09:30","end_time":"12:00"},{"name":"Jorge","start_time":"15:00","end_time":"16:00"},{"name":"Jorge","start_time":"17:00","end_time":"19:30"}],"friday":[{"name":"Jorge","start_time":"08:00","end_time":"09:00"},{"name":"Jorge","start_time":"09:30","end_time":"12:00"},{"name":"Jorge","start_time":"15:00","end_time":"16:00"},{"name":"Jorge","start_time":"17:00","end_time":"19:30"}]}');
	}
	catch(e){
		console.log(e);
	}
}

/**
 * [generateIntervalTimeArray obtiene un arraeglo de intervalor de tiempo]
 * @param  {int} startHour [hora de inicio del intervalo]
 * @param  {int} startMin  [minutos de la hora de inicio]
 * @param  {int} endHour   [Hora de termino del intervalo]
 * @param  {int} endMin    [Minutos de la hora de termino]
 * @return {array}           [description]
 */
function generateIntervalTimeArray(startHour,startMin,endHour,endMin){
	try{
		var d = new Date();
		var intervalMinutes = 30;
		d.setHours(startHour);
		d.setMinutes(startMin);
		var date = d.getDate();
		var timeArr = [];

		while ( d.getHours() <= endHour )
		{
		   var hours = d.getHours();
		   var minutes = d.getMinutes();
		   if(hours == endHour && minutes > endMin) break;
		   hours = ( "0" + hours ).slice(-2);
		   minute = ( "0" + d.getMinutes() ).slice(-2);
		   timeArr.push( hours + ":" + minute );
		   d.setMinutes( d.getMinutes() + intervalMinutes);
		}
		
		return timeArr;

	}
	catch(e){
		console.log(e);
	}
}

/**
 * [getDays Permite obtener un objeto con los dias de la semana]
 * @return {object} [Retorna un objeto con los dias de la semana]
 */
function getDays(){
	try{
		var days = {
			'monday': 'Lunes',
			'tuesday': 'Martes',
			'wednesday': 'Miércoles',
			'thursday': 'Jueves',
			'friday': 'Viernes',
			'saturday': 'Sábado',
			'sunday': 'Domingo',
		};
		return days;
	}
	catch(e){

	}
}