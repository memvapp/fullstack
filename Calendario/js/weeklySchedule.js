/**
 * [headerSchedule genera el header de la agenda semanal]
 * @return {string} [String con el html correspondiente al header de la agenda]
 */
function headerSchedule(){
	try{
		var html = "";
		var weeklyDays = getDays();

		html += '<div class="headerSchedule">';
		html += '<div class="col-md-1 col-sch headerSch hours"><i class="fa fa-angle-double-left" aria-hidden="true"></i></div>';
		
		$.each(weeklyDays, function(index, value){
			html += '<div class="col-md-1 col-sch headerSch '+index+'">'+value+'</div>';
		});
		
		html += '<div class="col-md-1 col-sch headerSch hours"><i class="fa fa-angle-double-right" aria-hidden="true"></i></div>';
		html += '</div>';

		return html;
	}
	catch(e){
		console.log(e);
	}
}

/**
 * [bodySchedule genera el cuerpo de la agenda semanal]
 * @return {string} [String con el html correspondiente al cuerpo de la semana]
 */
function bodySchedule(){
	try{
		var html = '';
		var hours = generateIntervalTimeArray(8,0,20,0);
		var weeklyDays = getDays();

		$.each(hours, function(index1, value1){
			html += '<div class="bodySchedule">'
			html += '<div class="col-md-1 col-sch hoursDetail block'+value1.replace(":", "")+'">'+value1+'</div>'
			
			$.each(weeklyDays, function(index2, value2){
				html += '<div class="col-md-1 col-sch bodySch"  id="'+index2+'block'+value1.replace(":", "")+'"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i></div>'
			});

			html += '<div class="col-md-1 col-sch hoursDetail block'+value1.replace(":", "")+'">'+value1+'</div>'
			html += '</div>'
		});

	  return html;
	}
	catch(e){
		console.log(e);
	}
}

/**
 * [chargeHoursTaken Se encarga de marcar las horas ocupadas en la agenda]
 * @return {void} [Reemplaza el contenido del div asociado a un bloque horario]
 */
function chargeHoursTaken(){
	var hoursTaken = getJsonData();

	$.each(hoursTaken, function(day, objectTime) {
		$.each(objectTime, function(index2, jsonObject) {

			startTime = jsonObject.start_time.split(":");
			endTime = jsonObject.end_time.split(":");
			scheduledHours = generateIntervalTimeArray(startTime[0],startTime[1],endTime[0],endTime[1]);
				
			$.each(scheduledHours, function(index, hours) {
				console.log(day);
				var classBlock = '#'+day+'block'+hours.split(":").join("");
				$(classBlock).addClass("selectedBlock");
				$(classBlock).html('<i class="fa fa-calendar-check-o" aria-hidden="true"></i> '+jsonObject.name);
			});
		});
	});
}


$(document).ready(function($) {

	var content = function(){
		var header = headerSchedule();
		var body = bodySchedule();
		return header+body;
	}
	//Se genera la agenda
	$('#content').html(content);
	//Se marcan los horarios que ya estan tomados
	chargeHoursTaken();
});