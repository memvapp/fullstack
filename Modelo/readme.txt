
##################################################

Modelo de datos

- ver el archivo techK_model.png

##################################################

SQL

Considerando el enunciado anterior conteste las siguientes preguntas:

1) Escriba una Query que entregue la lista de alumnos para el curso "programación"


SELECT est.id, est.name
FROM student AS est
INNER JOIN course_student AS cs ON est.id = cs.student_id
INNER JOIN course AS c ON cs.course_id = c.id
WHERE c.name = "programación"


2) Escriba una Query que calcule el promedio de notas de un alumno en un curso.


SELECT est.name, AVG(es.score) as avg
FROM exam_student AS es
INNER JOIN student AS est ON es.student_id = est.id
INNER JOIN exam AS ex ON es.exam_id = ex.id
INNER JOIN ourse AS c ON ex.course_id = c.id
WHERE est.name='mauricio' AND c.name = "programación"


3) Escriba una Query que entregue a los alumnos y el promedio que tiene en cada curso.

SELECT est.id AS estudiante_id ,est.name AS estudiante, c.name AS curso, AVG(es.nota) AS promedio
FROM exam_student AS es
INNER JOIN student AS est ON es.student_id = est.id
INNER JOIN exam AS ex ON es.exam_id = ex.id
INNER JOIN course_student AS cs ON est.id = cs.student_id 
INNER JOIN course AS c ON c.id = cs.course_id
GROUP BY est.name, course.name

4) Escriba una Query que lista a todos los alumnos con más de un curso con promedio rojo.

SELECT savg.id, savg.name FROM ( 
	SELECT est.id, est.name, c.name AS course_name, AVG(sc.score) AS avg 
	FROM student AS est 
	INNER JOIN exam_student AS sc ON est.id = sc.student_id 
	INNER JOIN exam AS ex ON sc.exam_id = ex.id 
	INNER JOIN course AS c ON ex.course_id = c.id 
	GROUP BY est.id, c.name 
	) AS savg 
GROUP BY savg.id 
HAVING SUM(if(savg.avg<4, 1, 0))>1

##################################################


Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis: PLAYERS(Nombre, Pais, Ranking). Suponga que Ranking es un número de 1 a 100 que es distinto para cada jugador. Si la tabla en un momento dado tiene solo 20 registros, indique cuantos registros tiene la tabla que resulta de la siguiente consulta:

SELECT c1.Nombre, c2.Nombre
FROM PLAYERS c1, PLAYERS c2
WHERE c1.Ranking > c2.Ranking

Seleccione las respuestas correctas:
a) 400
b) 190
c) 20
d) imposible saberlo


Respuesta: b) 190 (Sumatoria desde el 19 al 1)

La primera comparación c1.Ranking > c2.Ranking nos indica que, por ejemplo, el jugador con ranking 20 tiene 19 jugadores en mejor posición que él, es decir 19 registros. Para el siguiente ciclo habrán 18 jugadores en mejor posición que el número 19, y así sucesivamente.
