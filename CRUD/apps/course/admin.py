# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib import admin

from apps.course.models import Course

# Register your models here.

admin.site.register(Course)