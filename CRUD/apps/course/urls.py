# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.conf.urls import url, include

from apps.course.views import CourseList, CourseCreate, CourseUpdate, CourseDelete, index

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^nuevo$', CourseCreate.as_view(), name='course_create'),
    url(r'^listar', CourseList.as_view(), name='course_list'),
    url(r'^editar/(?P<pk>\d+)/$', CourseUpdate.as_view(), name='course_edit'),
    url(r'^eliminar/(?P<pk>\d+)/$', CourseDelete.as_view(), name='course_delete'),
]