# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django import forms

from apps.exam.models import Exam, Score



class ExamForm(forms.ModelForm):

	class Meta:
		model = Exam
		fields = [
			'title',
			'Course'
		]
		labels = {
			'titulo': 'Titulo',
			'cursos': 'Cursos'
		}
		widgets = {
			'title':forms.TextInput(attrs={'class':'form-control'}),
			'Course':forms.Select(attrs={'class':'form-control'}),
		}


class ScoreForm(forms.ModelForm):

	class Meta:
		model = Score
		fields = [
			'score',
			'Exam',
			'Student',
		]
		labels = {
			'nota': 'Nota',
			'prueba': 'Prueba',
			'estudiante': 'Estudiante',			
		}
		widgets = {
			'score':forms.TextInput(attrs={'class':'form-control'}),
			'Exam':forms.Select(attrs={'class':'form-control'}),
			'Student':forms.Select(attrs={'class':'form-control'}),
		}
