# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from apps.student.forms import StudentForm
from apps.student.models import Student

def index(request):
	return render(request, 'student/index.html')

class StudentList(ListView):
	model = Student
	template_name = 'student/student_list.html'
	paginate_by = 10

class StudentCreate(CreateView):
	model = Student
	form_class = StudentForm
	template_name = 'student/student_form.html'
	success_url = reverse_lazy('student:student_list')


class StudentUpdate(UpdateView):
	model = Student
	form_class = StudentForm
	template_name = 'student/student_form.html'
	success_url = reverse_lazy('student:student_list')


class StudentDelete(DeleteView):
	model = Student
	template_name = 'student/student_delete.html'
	success_url = reverse_lazy('student:student_list')

#Listado de alumnos y su promedio
def StudentAverage(request):
	estudents = Student.objects.raw('SELECT est.id, est.name, ROUND(AVG(sc.score),1) AS avg FROM student_student AS est INNER JOIN exam_score AS sc ON est.id = sc.student_id GROUP BY est.id, est.name')
	contexto = {'object_list':estudents}
	return render(request, 'student/student_average.html', contexto)

#Listado de alumnos con mas de un promedio rojo
def StudentRedAverage(request):
	estudents = Student.objects.raw('SELECT savg.id, savg.name FROM ( SELECT est.id, est.name, c.name AS course_name, AVG(sc.score) AS avg FROM student_student AS est INNER JOIN exam_score AS sc ON est.id = sc.Student_id INNER JOIN exam_exam AS ex ON sc.Exam_id = ex.id INNER JOIN course_course AS c ON ex.Course_id = c.id GROUP BY est.id, c.name ) AS savg GROUP BY savg.id HAVING SUM(if(savg.avg<4, 1, 0))>1')
	contexto = {'object_list':estudents}
	return render(request, 'student/student_red_average.html', contexto)