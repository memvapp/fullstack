# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models

# Create your models here.

class Course(models.Model):
	name = models.CharField(max_length=255)
	
	def __str__(self):
		return '{}'.format(self.name)