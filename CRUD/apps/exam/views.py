# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from apps.exam.forms import ExamForm, ScoreForm
from apps.exam.models import Exam, Score

from apps.course.forms import CourseForm
from apps.course.models import Course

from apps.student.forms import StudentForm
from apps.student.models import Student
# Create your views here.

# Exam Views
def index(request):
	return render(request, 'exam/index.html')


class ExamList(ListView):
	model = Exam
	template_name = 'exam/exam_list.html'
	paginate_by = 10

class ExamCreate(CreateView):
	model = Exam
	template_name = 'exam/exam_form.html'
	form_class = ExamForm
	success_url = reverse_lazy('exam:exam_list')

class ExamUpdate(UpdateView):
	model = Exam
	second_model = Course
	template_name = 'exam/exam_form.html'
	form_class = ExamForm
	second_form_class = CourseForm
	success_url = reverse_lazy('exam:exam_list')


class ExamDelete(DeleteView):
	model = Exam
	template_name = 'exam/exam_delete.html'
	success_url = reverse_lazy('exam:exam_list')


#Score Views

class ScoreList(ListView):
	model = Score
	template_name = 'score/score_list.html'
	paginate_by = 10

class ScoreCreate(CreateView):
	model = Score
	template_name = 'score/score_form.html'
	form_class = ScoreForm
	success_url = reverse_lazy('exam:score_list')

class ScoreUpdate(UpdateView):
	model = Score
	template_name = 'score/score_form.html'
	form_class = ScoreForm
	success_url = reverse_lazy('exam:score_list')

class ScoreDelete(DeleteView):
	model = Score
	template_name = 'score/score_delete.html'
	success_url = reverse_lazy('exam:score_list')