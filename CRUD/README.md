# COLLEGE SYSTEM #

Descripción: Aplicación de gestión para una institución educacional que contempla la gestión de estudiantes, cursos, pruebas y notas

Versión: alpha 1.0 03/08/2017

#Instalación

#####1) Instalar python 3.6.2: https://www.python.org/


#####2) Instalar Django 1.11.3:

	En la linea de comandos ejecutar la instrucción => python -m pip instal django == 1.11.3

#####3) Instalar MySql: https://www.mysql.com/downloads/

#####4) Instalar virtual enviroment:
	Ejecutar el comando => python -m pip install virtualenv

#####5) Crear entorno virtual:
	Crear un directorio en la carpeta de usuario. escribir el comando cd ~
	python -m venv NOMBRE_AMBIENTE
	Posicionarse en el directorio Scripts dentro del directorio general del entorno virtual y escribir el comando "activate" , sin las comillas.
	Instalar django en el entorno virtual => python -m pip instal django == 1.11.3

#####6) Carpeta del proyecto:
	La carpeta debe estar en la raiz de la carpeta creada por el entorno virtual con el nombre collegesystem	

#####7) Configurar conexión a la base de datos en el archivo collegeSystem/settings.py DATABASES => NAME, USER, PASS y PORT
	La configuración por defecto es NAME: collegeDB, USER: root,PASS: '', PORT: 3306

#####8) Ejecutar las migraciones para crear las tablas en la base de datos, para ello, dentro del proyecto ejecutar:
	- python manage.py makemigrations
	- python manage.py migrate

#####9) Crear un super usuario para poder gestionar el modelo desde el admin de Django. Para ello ejecutar el comando:
	- python manage.py createsuperuser
	- Seguir las instrucciones y listo.

#####10) Por último correr el servidor. Para ello ejecutar el comando:
	- python manage.py runserver

#####11) Para ver la aplicación dirigirse a localhost:8000 (localhost:8000/admin para ver el administrador de Django)


#Supuesto del proyecto
	- Se considera como nota roja, para los resportes de promedio y alumnos con más de un curso en rojo, cualquier nota bajo un 4
	- No se consideró el modelo para los profesores aunque se puede incorporar.


#Contacto

Creado por maumunozvalencia@gmail.com
fono +569 87584218