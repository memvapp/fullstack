# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models

from apps.course.models import Course

# Create your models here.

class Student(models.Model):
	name = models.CharField(max_length=100)
	Course = models.ManyToManyField(Course, blank=True)
	
	def __str__(self):
		return '{}'.format(self.name)